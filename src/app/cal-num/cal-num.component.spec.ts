import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalNumComponent } from './cal-num.component';

describe('CalNumComponent', () => {
  let component: CalNumComponent;
  let fixture: ComponentFixture<CalNumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalNumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalNumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
