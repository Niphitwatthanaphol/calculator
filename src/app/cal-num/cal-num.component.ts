import { Component, OnInit, Input } from '@angular/core';
import { callLifecycleHooksChildrenFirst } from '@angular/core/src/view/provider';

@Component({
  selector: 'app-cal-num',
  templateUrl: './cal-num.component.html',
  styleUrls: ['./cal-num.component.css']
})

export class CalNumComponent implements OnInit {
  
  constructor() {   
  }
  ngOnInit() {
  }
  
  sum='';

  call='';
  
  onClick1() {
    this.call += '1';
  }
  onClick2() {
   
    this.call += '2';
  }
  onClick3() {
    this.call += '3';
  }
  onClick4() {
    this.call += '4';
  }
  onClick5() {
    this.call += '5';
  }
  onClick6() {
    this.call += '6';
  }
  onClick7() {
    this.call += '7';
  }
  onClick8() {
    this.call += '8';
  }
  onClick9() {
    this.call += '9';
  }
  onClick0() {
    this.call += '0';
  }
  onClickLeft() {
    this.call += '(';
  }
  onClickRight() {
    this.call += ')';
  }
  onClickDot() {
    this.call += '.';
  }
  onClickPlus() {
   
    this.call += '+';
  }
  onClickMinus() {
    this.call += '-';
  }
  onClickTimes() {
    this.call += '*';
  }
  onClickDivide() {
    this.call += '/';
  }
  onClickSquare() {
    this.call += '^2';
  }

  Sum(){
   
    
  }
  Clear(){
   
    
  }
}
