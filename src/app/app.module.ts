import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalNumComponent } from './cal-num/cal-num.component';
import { CalSymbolComponent } from './cal-symbol/cal-symbol.component';

@NgModule({
  declarations: [
    AppComponent,
    CalNumComponent,
    CalSymbolComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
